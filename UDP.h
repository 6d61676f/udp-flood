#ifndef UDP_H
#define UDP_H
#include <arpa/inet.h>
#include <chrono>
#include <cstdbool>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <random>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

class UDP
{

private:
  struct sockaddr_in adresa;
  struct udphdr* udpHDR;
  struct iphdr* ipHDR;
  const char* pachet = "W0l0l0 Fraere! h4ck";
  const ssize_t lungimeIP = sizeof(struct iphdr);
  const ssize_t lungimeUDP = sizeof(struct udphdr);
  const ssize_t lungimePayload = strlen(pachet) + 1;
  const ssize_t lungimePachet = lungimeIP + lungimeUDP + lungimePayload;
  uint8_t* head;
  bool srcOK = false;
  bool dstOK = false;
  void initHeaders();
  bool send();
  int socketFD;

public:
  UDP(const char* ipSRC, const char* ipDST);
  UDP();
  ~UDP();
  bool setIp(const char* ipSRC, const char* ipDST);
  void Flood(uint64_t milisecunde);
};

#endif // UDP_H
